## 1. Intro

Performs basic mathematical operations:

- summation `+`
- subtraction `-`
- multiplication `*`
- division `/`
- exponentiation `^`
- parentheses `(...)`

API:

- `isExpressionValid(string)` boolean
- `parseAndEvaluate(string)` number

## 2. Usage

- import `AngularBasicCalculatorModule` into a Single Page App

```javascript

/*
  src/app/app.module.ts
*/

import { AngularBasicCalculatorModule } from 'angular-basic-calculator';

@NgModule({
  imports: [
    ...,
    AngularBasicCalculatorModule
  ],
  ...
})
export class AppModule { }
```

- inject `AngularBasicCalculatorService` into a component/service/pipe

```javascript

constructor(private _angularBasicCalculatorService: AngularBasicCalculatorService) {
  this._angularBasicCalculatorService.parseAndEvaluate('1 + 1'); // = 2
}
```

## 3. Demo in a Single Page App

- See [usage](https://bitbucket.org/kdha200501/jacks-angular6-demo-app/src/master/src/calculator/app-effects.service.ts#lines-86) in the demo app

- See live demo of the demo app on [CodePen](https://codepen.io/kdha200501/project/full/XbMdQM/)

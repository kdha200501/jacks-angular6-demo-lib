import { TestBed } from '@angular/core/testing';

import { AngularBasicCalculatorService } from './angular-basic-calculator.service';

describe('AngularBasicCalculatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [AngularBasicCalculatorService]
  }));

  it('should be created', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    expect(service).toBeTruthy();
  });

  it('treats special characters as part of operand', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    let result = service.splitToken(' 1234567890~!@#$%_`={}[]\:";\'<>?,.');
    expect(result.length).toEqual(1);
  });

  it('should invalidate invalid operand', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    expect( service.isExpressionValid('123') ).toEqual(true);
    expect( service.isExpressionValid(' 123') ).toEqual(true);
    expect( service.isExpressionValid('1 23') ).toEqual(false);
    expect( service.isExpressionValid('12 3') ).toEqual(false);
    expect( service.isExpressionValid('123 ') ).toEqual(true);
    expect( service.isExpressionValid(' 123 ') ).toEqual(true);
  });

  it('invalidates an empty expression', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    expect( service.isExpressionValid('') ).toEqual(false);
  });

  it('invalidates an expression with consecutive operators', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    expect( service.isExpressionValid('+') ).toEqual(false);
    expect( service.isExpressionValid('+-') ).toEqual(false);
    expect( service.isExpressionValid(' +-') ).toEqual(false);
    expect( service.isExpressionValid('+ -') ).toEqual(false);
    expect( service.isExpressionValid('+- ') ).toEqual(false);
    expect( service.isExpressionValid('1++') ).toEqual(false);
    expect( service.isExpressionValid(' 1++') ).toEqual(false);
    expect( service.isExpressionValid('1 ++') ).toEqual(false);
    expect( service.isExpressionValid('1+ +') ).toEqual(false);
    expect( service.isExpressionValid('1++ ') ).toEqual(false);
    expect( service.isExpressionValid('1+2++') ).toEqual(false);
    expect( service.isExpressionValid(' 1+2++') ).toEqual(false);
    expect( service.isExpressionValid('1 +2++') ).toEqual(false);
    expect( service.isExpressionValid('1+ 2++') ).toEqual(false);
    expect( service.isExpressionValid('1+2 ++') ).toEqual(false);
    expect( service.isExpressionValid('1+2+ +') ).toEqual(false);
    expect( service.isExpressionValid('1+2++ ') ).toEqual(false);
  });

  it('ignores space in an expression', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    expect( service.isExpressionValid('1+2') ).toEqual(true);
    expect( service.isExpressionValid(' 1+2') ).toEqual(true);
    expect( service.isExpressionValid('1 +2') ).toEqual(true);
    expect( service.isExpressionValid('1+ 2') ).toEqual(true);
    expect( service.isExpressionValid('1+2 ') ).toEqual(true);
    expect( service.isExpressionValid(' 1 +2') ).toEqual(true);
    expect( service.isExpressionValid(' 1 + 2') ).toEqual(true);
    expect( service.isExpressionValid(' 1 + 2 ') ).toEqual(true);

    expect( service.isExpressionValid('1-2') ).toEqual(true);
    expect( service.isExpressionValid(' 1-2') ).toEqual(true);
    expect( service.isExpressionValid('1 -2') ).toEqual(true);
    expect( service.isExpressionValid('1- 2') ).toEqual(true);
    expect( service.isExpressionValid('1-2 ') ).toEqual(true);
    expect( service.isExpressionValid(' 1 -2') ).toEqual(true);
    expect( service.isExpressionValid(' 1 - 2') ).toEqual(true);
    expect( service.isExpressionValid(' 1 - 2 ') ).toEqual(true);

    expect( service.isExpressionValid('1*2') ).toEqual(true);
    expect( service.isExpressionValid(' 1*2') ).toEqual(true);
    expect( service.isExpressionValid('1 *2') ).toEqual(true);
    expect( service.isExpressionValid('1* 2') ).toEqual(true);
    expect( service.isExpressionValid('1*2 ') ).toEqual(true);
    expect( service.isExpressionValid(' 1 *2') ).toEqual(true);
    expect( service.isExpressionValid(' 1 * 2') ).toEqual(true);
    expect( service.isExpressionValid(' 1 * 2 ') ).toEqual(true);

    expect( service.isExpressionValid('1/2') ).toEqual(true);
    expect( service.isExpressionValid(' 1/2') ).toEqual(true);
    expect( service.isExpressionValid('1 /2') ).toEqual(true);
    expect( service.isExpressionValid('1/ 2') ).toEqual(true);
    expect( service.isExpressionValid('1/2 ') ).toEqual(true);
    expect( service.isExpressionValid(' 1 /2') ).toEqual(true);
    expect( service.isExpressionValid(' 1 / 2') ).toEqual(true);
    expect( service.isExpressionValid(' 1 / 2 ') ).toEqual(true);
  });

  it('determines if an expression is complete', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    expect( service.isExpressionComplete('123') ).toEqual(true);
    expect( service.isExpressionComplete('1 + 2') ).toEqual(true);
    // expect( service.isExpressionComplete('+') ).toEqual(false);
    // expect( service.isExpressionComplete('+ - * / ^') ).toEqual(false);
    expect( service.isExpressionComplete('1 + ') ).toEqual(false);
    expect( service.isExpressionComplete('1 + 2 -') ).toEqual(false);
    expect( service.isExpressionComplete('1 + 2 - 3 *') ).toEqual(false);
    expect( service.isExpressionComplete('1 + 2 - 3 * 4 /') ).toEqual(false);
    expect( service.isExpressionComplete('1 + 2 - 3 * 4 / 5 ^') ).toEqual(false);
    expect( service.isExpressionComplete('(1 + ) - 3 * 4 / 5 ^ 6') ).toEqual(false);
    expect( service.isExpressionComplete('1 + (2 - ) * 4 / 5 ^ 6') ).toEqual(false);
    expect( service.isExpressionComplete('1 + 2 - (3 * ) / 5 ^ 6') ).toEqual(false);
    expect( service.isExpressionComplete('1 + 2 - 3 * (4 / ) ^ 6') ).toEqual(false);
    expect( service.isExpressionComplete('1 + 2 - 3 * 4 / (5 ^ )') ).toEqual(false);
  });

  it('treats incomplete expression as valid', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    expect( service.isExpressionValidButIncomplete('1 + ') ).toEqual(false);
    expect( service.isExpressionValidButIncomplete('1 + 2 -') ).toEqual(false);
    expect( service.isExpressionValidButIncomplete('1 + 2 - 3 *') ).toEqual(false);
    expect( service.isExpressionValidButIncomplete('1 + 2 - 3 * 4 /') ).toEqual(false);
    expect( service.isExpressionValidButIncomplete('1 + 2 - 3 * 4 / 5 ^') ).toEqual(false);
    expect( service.isExpressionValidButIncomplete('(1 + ) - 3 * 4 / 5 ^ 6') ).toEqual(false);
    expect( service.isExpressionValidButIncomplete('1 + (2 - ) * 4 / 5 ^ 6') ).toEqual(false);
    expect( service.isExpressionValidButIncomplete('1 + 2 - (3 * ) / 5 ^ 6') ).toEqual(false);
    expect( service.isExpressionValidButIncomplete('1 + 2 - 3 * (4 / ) ^ 6') ).toEqual(false);
    expect( service.isExpressionValidButIncomplete('1 + 2 - 3 * 4 / (5 ^ )') ).toEqual(false);
  });

  it('returns the root node\'s key when the expression is a single operand', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    expect( service.parseAndEvaluate('123') ).toEqual(123);
  });

  it('perform math operations', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    expect( service.parseAndEvaluate('1 + 2') ).toEqual(1 + 2);
    expect( service.parseAndEvaluate('3 - 4') ).toEqual(3 - 4);
    expect( service.parseAndEvaluate('5 * 6') ).toEqual(5 * 6);
    expect( service.parseAndEvaluate('7 / 8') ).toEqual(7 / 8);
    expect( service.parseAndEvaluate('9 ^ 10') ).toEqual( Math.pow(9, 10) );
  });

  it('respects operator precedence', () => {
    const service: AngularBasicCalculatorService = TestBed.get(AngularBasicCalculatorService);
    expect( service.parseAndEvaluate('1 + 2 * 3') ).toEqual( 1 + (2 * 3) );
    expect( service.parseAndEvaluate('1 + 2 / 3') ).toEqual( 1 + (2 / 3) );
    expect( service.parseAndEvaluate('1 + 2 ^ 3') ).toEqual( 1 + Math.pow(2, 3) );
    expect( service.parseAndEvaluate('1 - 2 * 3') ).toEqual( 1 - (2 * 3) );
    expect( service.parseAndEvaluate('1 - 2 / 3') ).toEqual( 1 - (2 / 3) );
    expect( service.parseAndEvaluate('1 - 2 ^ 3') ).toEqual( 1 - Math.pow(2, 3) );
    expect( service.parseAndEvaluate('(1 + 2) * 3') ).toEqual( (1 + 2) * 3 );
    expect( service.parseAndEvaluate('(1 + 2) / 3') ).toEqual( (1 + 2) / 3 );
    expect( service.parseAndEvaluate('(1 + 2) ^ 3') ).toEqual( Math.pow(1 + 2, 3) );
    expect( service.parseAndEvaluate('(1 - 2) * 3') ).toEqual( (1 - 2) * 3 );
    expect( service.parseAndEvaluate('(1 - 2) / 3') ).toEqual( (1 - 2) / 3 );
    expect( service.parseAndEvaluate('(1 - 2) ^ 3') ).toEqual( Math.pow(1 - 2, 3) );
  });
});

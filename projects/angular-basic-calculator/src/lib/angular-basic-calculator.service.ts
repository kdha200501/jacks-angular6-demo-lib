import { Injectable } from '@angular/core';
import { Parser } from '../classes/Parser';
import { Alphanumeric } from '../classes/Node/AlphanumericNode';
import { MathExpressionNode } from '../classes/Node/MathExpressionNode';
import { MathExpressionTree } from '../classes/Tree/MathExpressionTree';
import { MathOperationEnum, MathOperationWeight, tokenRegExp } from '../classes/Expression/MathExpression';
import { Stack } from '../classes/Stack';

const filterNonEmptyToken = (token: string) => !!token;
const regExpNonDigit = /[^0-9]+/;
const validateFloat = tokenList => tokenList.length > 2 ? false : !tokenList.reduce( (op, item) => op || regExpNonDigit.test(item), false );
const isFloat = token => validateFloat( token.trim().split('.') );

@Injectable()
export class AngularBasicCalculatorService extends Parser<Alphanumeric, MathExpressionNode, MathExpressionTree> {

  constructor() {
    super(MathExpressionTree);
  }

  // Implement Protected Abstract Variables
  protected get openParenthesis(): string {
    return MathOperationEnum.OPEN_PARENTHESIS;
  }

  protected get closeParenthesis(): string {
    return MathOperationEnum.CLOSE_PARENTHESIS;
  }

  // Implement Abstract Methods
  public splitToken (expressionStr: string): Array<string> {
    return expressionStr.split(tokenRegExp).filter(filterNonEmptyToken);
  }

  protected buildOperatorNode(operator: string, nodeStack: Stack<MathExpressionNode>): void {
    // the top node of an expression sub-tree must be an operator
    let node = new MathExpressionNode(operator);
    node.right = nodeStack.pop();
    node.left = nodeStack.pop();
    nodeStack.push(node);
  }

  protected buildOperandNode(token: string, nodeStack: Stack<MathExpressionNode>): void {
    let operand = isFloat(token) ? parseFloat(token) : NaN;
    nodeStack.push( new MathExpressionNode(operand) );
  }

  protected tokenIsAnOperator(token: string): boolean {
    return !!MathOperationWeight[token];
  }

  protected tokenIsLowerPrecedence(a: string, b: string): boolean {
    return MathOperationWeight[a] <= MathOperationWeight[b];
  }

  // Public Methods
  public isExpressionValid(expressionStr: string): boolean {
    return this.parseInfixExpression(expressionStr).traverseToEvaluateMathExpression.verify();
  }

  public isExpressionComplete(expressionStr: string): boolean {
    return this.parseInfixExpression(expressionStr).traverseToEvaluateMathExpression.validate();
  }

  public isExpressionValidButIncomplete(expressionStr: string): boolean {
    return this.isExpressionValid(expressionStr) && !this.isExpressionComplete(expressionStr);
  }

  public parseAndEvaluate(expressionStr: string): number {
    return this.parseInfixExpression(expressionStr).traverseToEvaluateMathExpression.solve();
  }

}

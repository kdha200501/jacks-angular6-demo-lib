import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularBasicCalculatorComponent } from './angular-basic-calculator.component';

describe('AngularBasicCalculatorComponent', () => {
  let component: AngularBasicCalculatorComponent;
  let fixture: ComponentFixture<AngularBasicCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngularBasicCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularBasicCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { AngularBasicCalculatorComponent } from './angular-basic-calculator.component';
import { AngularBasicCalculatorService } from './angular-basic-calculator.service';

@NgModule({
  imports: [
  ],
  declarations: [AngularBasicCalculatorComponent],
  exports: [AngularBasicCalculatorComponent],
  providers: [AngularBasicCalculatorService]
})
export class AngularBasicCalculatorModule { }

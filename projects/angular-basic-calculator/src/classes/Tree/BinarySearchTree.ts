import { Tree } from './Tree';
import { NumericNode } from '../Node/NumericNode';

export class BinarySearchTree extends Tree<number, NumericNode>{

    private static insertRec(node: NumericNode, key: number) {
        if (node.isNull) {
            node.key = key;
            return node;
        }

        else if (key < node.key) {
            node.left = BinarySearchTree.insertRec(<NumericNode>node.left, key);
        }
        // endIf the key to be inserted belongs to the left sub-tree

        else if (key > node.key) {
            node.right = BinarySearchTree.insertRec(<NumericNode>node.right, key);
        }
        // endIf the key to be inserted belongs to the right sub-tree

        return node;
    }

    private static searchRec(node: NumericNode, key: number) {
        if (node.isNull || node.key === key) {
            return node;
        }
        return key < node.key ?
            BinarySearchTree.searchRec(<NumericNode>node.left, key) :
            BinarySearchTree.searchRec(<NumericNode>node.right, key);
    }

    private static searchMinRec(node: NumericNode): NumericNode {
        return node.left.isNull ? node : BinarySearchTree.searchMinRec(<NumericNode>node.left);
    }

    private static searchToRemoveRec(node: NumericNode, key: number) {
        if (node.isNull) {
            // return null node as a way to remove a node
            return node;
        }

        if (key < node.key) {
            node.left = BinarySearchTree.searchToRemoveRec(<NumericNode>node.left, key);
        }
        // endIf the key to be overwritten belongs to the left sub-tree

        else if (key > node.key) {
            node.right = BinarySearchTree.searchToRemoveRec(<NumericNode>node.right, key);
        }
        // endIf the key to be overwritten belongs to the right sub-tree

        else {
            if (node.left.isNull) {
                // if the node has no child, then its right child is null -> the node will be nullified upon return
                // if the node has an only-child, then its right child replaces its place upon return
                return node.right;
            }
            // endIf the node bearing the key has no child or left child, only

            else if (node.right.isNull) {
                return node.left;
            }
            // endIf the node bearing the key has no child or right child, only

            else {
                let inOrderSuccessor = BinarySearchTree.searchMinRec(<NumericNode>node.right);
                // overwrite its value with its in-order successor's key
                node.overwriteKey(inOrderSuccessor.key);
                // remove the leave node
                inOrderSuccessor.nullify();
            }
            // endIf the node bearing the key has two child nodes
        }
        // endIf the key to be overwritten is located

        return node;
    }

    private static bisecRec(nodeList: Array<NumericNode>, fromAndIncluding: number, toButNotIncluding: number): NumericNode {
        let mid = Math.floor( (toButNotIncluding + fromAndIncluding) / 2 );

        if (mid > -1 && mid < toButNotIncluding) {
            let node = new NumericNode(nodeList[mid].key);
            node.left = BinarySearchTree.bisecRec(nodeList, fromAndIncluding, mid);
            node.right = BinarySearchTree.bisecRec(nodeList, mid + 1, toButNotIncluding);
            return node;
        }
        // endIf can slice further

        else {
            return new NumericNode(); // return null node
        }
        // endIf cannot slice further
    }

    constructor() {
        super(NumericNode);
    }

    // Public Methods
    public insert(key: number): void {
        BinarySearchTree.insertRec(this.root, key);
    }

    public insertBatch(...keyList: Array<number>): void {
        keyList.forEach( key => this.insert(key) );
    }

    public search(key: number): NumericNode {
        return BinarySearchTree.searchRec(this.root, key);
    }

    public remove(key: number): void {
        BinarySearchTree.searchToRemoveRec(this.root, key);
    }

    public balance(): void {
        let inOrderTraversal: Array<NumericNode> = this.traverseToPrint.exportInOrder();
        this._root = BinarySearchTree.bisecRec(inOrderTraversal, 0, inOrderTraversal.length);
    }
}

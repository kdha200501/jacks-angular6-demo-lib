import { NullableNode } from '../Node/NullableNode';
import { TraverseToPrint } from '../Traversal/TraverseToPrint';

/*
    T - the type of a generic NullableNode's key
    K - a generic NullableNode
*/
export abstract class Tree<T, K extends NullableNode<T>> {
    protected constructor(nodeClass: {new(): K}) {
        this._root = new nodeClass();
        this._traverseToPrint = new TraverseToPrint<T, K>();
        this._traverseToPrint.tree = this;
    }

    // Private Variables
    protected _root: K;
    private _traverseToPrint: TraverseToPrint<T, K>;

    // Private Methods
    private heightRec(node: K): number {
        return node.isNull ?
            0 :
            Math.max( this.heightRec(<K>node.left), this.heightRec(<K>node.right) ) + 1;
    }

    // Accessors
    public get root(): K {
        return this._root;
    }

    public set root(node: K) {
        this._root = node;
    }

    public get height(): number {
        return this.heightRec(this.root);
    }

    public get traverseToPrint(): TraverseToPrint<T, K> {
        return this._traverseToPrint;
    }
}

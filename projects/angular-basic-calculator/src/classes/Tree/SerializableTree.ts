import { SerializableNode } from '../Node/SerializableNode';
import { TraverseToSerialize } from '../Traversal/TraverseToSerialize';
import { Tree } from './Tree';

/*
    T - the type of a generic SerializableNode's key
    K - a generic SerializableNode
*/
export abstract class SerializableTree<T, K extends SerializableNode<T>> extends Tree<T, K>{
    protected constructor(nodeClass: {new(): K}) {
        super(nodeClass);
        this._traverseToSerialize = new TraverseToSerialize<T, K>();
        this._traverseToSerialize.tree = this;
    }

    // Private Variables
    private _traverseToSerialize: TraverseToSerialize<T, K>;

    // Accessors
    public get traverseToSerialize(): TraverseToSerialize<T, K> {
        return this._traverseToSerialize;
    }
}

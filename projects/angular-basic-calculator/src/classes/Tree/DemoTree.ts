import { SerializableTree } from './SerializableTree';
import { Alphanumeric, AlphanumericNode } from '../Node/AlphanumericNode';

export class DemoTree extends SerializableTree<Alphanumeric, AlphanumericNode>{
    constructor() {
        super(AlphanumericNode);
    }
}

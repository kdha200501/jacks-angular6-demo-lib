import { Traversal } from './Traversal';
import { NullableNode } from '../Node/NullableNode';

export class TraverseToExportArray<T, K extends NullableNode<T>> extends Traversal<T, K>{
    constructor() {
        super();
    }

    // Implement Abstract Methods
    protected onTraverseLevelOrderRec(node: K, ...argList) {
        let [output] = argList;
        output.push(node);
    }

    protected onTraversePreOrderRec(node: K, ...argList) {
        let [output] = argList;
        output.push(node);
    }

    protected onTraverseInOrderRec(node: K, ...argList) {
        let [output] = argList;
        output.push(node);
    }

    protected onTraversePostOrderRec(node: K, ...argList) {
        let [output] = argList;
        output.push(node);
    }

    // Public Methods
    public exportLevelOrder(): Array<K> {
        let output: Array<K> = [];
        this.traverseLevelOrder(output);
        return output;
    }

    public exportPreOrder(): Array<K> {
        let output: Array<K> = [];
        this.traversePreOrder(output);
        return output;
    }

    public exportInOrder(): Array<K> {
        let output: Array<K> = [];
        this.traverseInOrder(output);
        return output;
    }

    public exportPostOrder(): Array<K> {
        let output: Array<K> = [];
        this.traversePostOrder(output);
        return output;
    }
}

import { Traversal } from './Traversal';
import { Alphanumeric } from '../Node/AlphanumericNode';
import { MathExpressionNode } from '../Node/MathExpressionNode';
import { MathOperationEnum, MathOperationWeight } from '../Expression/MathExpression';
import { Stack } from '../Stack';

export class TraverseToEvaluateMathExpression extends Traversal<Alphanumeric, MathExpressionNode> {
    constructor() {
        super();
    }

    // Implement Abstract Methods
    protected onTraverseLevelOrderRec(node: MathExpressionNode, ...argList) {
        let [output] = argList;

        if ( !MathOperationWeight[node.key] && !isFinite(<number>node.key) ) {
            output.push(node);
        }
        // endIf node is an operator parsed as an operand
    }

    protected onTraversePreOrderRec(node: MathExpressionNode, ...argList) {
        let [output] = argList;
        if (!node.isOperand) {
            output.push(node);
            return;
        }
        // endIf the node is an operator

        let top = <MathExpressionNode>output.peek();
        if (!top || !top.isOperand) {
            output.push(node);
            return;
        }
        // endIf the node is the root or node's previous traversal is a parent i.e. the node is a left operand

        let leftOperand = <MathExpressionNode>output.pop();
        let operator = <MathExpressionNode>output.pop();
        top = <MathExpressionNode>output.peek();
        let operation = new MathExpressionNode(`(${leftOperand.key}${operator.key}${node.key})`);

        if (top && top.isOperand) {
            this.onTraversePreOrderRec(operation, output);
        }
        // endIf the node's parent has a sibling operand

        else {
            output.push(operation);
        }
        // endIf the node's parent is the root note
    }

    protected onTraverseInOrderRec(node: MathExpressionNode, ...argList) {
        if (node.isValid) {
            return;
        }
        let [output] = argList;
        output.push(node);
    }

    protected onTraversePostOrderRec(node: MathExpressionNode, ...argList) {
        let [output] = argList;

        if (node.isOperand) {
            output.push(node);
            return;
        }

        let rightOperand = <number>(<MathExpressionNode>output.pop()).key;
        let leftOperand = <number>(<MathExpressionNode>output.pop()).key;
        let result = new MathExpressionNode();
        switch (node.key) {
            case MathOperationEnum.PLUS: {
                result.key = leftOperand + rightOperand;
                break;
            }
            case MathOperationEnum.MINUS: {
                result.key = leftOperand - rightOperand;
                break;
            }
            case MathOperationEnum.MULTIPLY: {
                result.key = leftOperand * rightOperand;
                break;
            }
            case MathOperationEnum.DIVIDE: {
                result.key = leftOperand / rightOperand;
                break;
            }
            case MathOperationEnum.POWER: {
                result.key = Math.pow(leftOperand, rightOperand);
                break;
            }
        }
        output.push(result);
    }

    // Protected Methods
    protected validateLevelOrder(): boolean {
        let output = new Stack<MathExpressionNode>();
        try {
            this.traverseLevelOrder(output);
            return !output.pop();
        }
        catch (e) {
            return false;
        }
    }

    protected verifyInOrder(): boolean {
        let output = new Stack<MathExpressionNode>();
        try {
            this.traverseInOrder(output);
            return !output.pop();
        }
        catch (e) {
            return false;
        }
    }

    protected formatPreOrder(): string {
        let output = new Stack<MathExpressionNode>();
        this.traversePreOrder(output);
        return <string>output.pop().key;
    }
    
    protected evaluatePostOrder(): number {
        let output = new Stack<MathExpressionNode>();
        this.traversePostOrder(output);
        return <number>output.pop().key;
    }

    // Public Methods
    public validate(): boolean {// to see if an expression is complete
        return !this.tree.root.left || !this.tree.root.left ? false : this.validateLevelOrder();
    }

    public verify(): boolean {// to see if all leave nodes are operands, all parent nodes are operator
        return !!this.tree.root && this.verifyInOrder();
    }

    public format(): string {
        return this.formatPreOrder();
    }

    public solve(): number {
        return this.evaluatePostOrder();
    }
}

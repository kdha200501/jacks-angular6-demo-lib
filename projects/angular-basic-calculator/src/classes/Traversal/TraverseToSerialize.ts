import { Traversal } from './Traversal';
import { SerializableNode } from '../Node/SerializableNode';
import { Counter } from '../Counter';

interface ISerialization {
    id: number;
    key: any;
}

export class TraverseToSerialize<T, K extends SerializableNode<T>> extends Traversal<T, K>{

    // Algorithm copied from BST insertRec
    private static deserializeRec<T, K extends SerializableNode<T>>(node: K, serialization: ISerialization) {
        if (node.isNull) {
            node.key = serialization.key;
            node.id = serialization.id;
            return node;
        }

        else if (serialization.id < node.id) {
            node.left = TraverseToSerialize.deserializeRec(<K>node.left, serialization);
        }
        // endIf the key to be inserted belongs to the left sub-tree

        else if (serialization.id > node.id) {
            node.right = TraverseToSerialize.deserializeRec(<K>node.right, serialization);
        }
        // endIf the key to be inserted belongs to the right sub-tree

        return node;
    }

    public static deserialize<T, K extends SerializableNode<T>>(node: K, serializationList: Array<ISerialization>): void {
        serializationList.forEach( serialization => TraverseToSerialize.deserializeRec(node, serialization) );
    }
    
    constructor() {
        super();
    }

    // Implement Abstract Methods
    protected onTraverseLevelOrderRec(node: K, ...argList) {
    }

    protected onTraversePreOrderRec(node: K, ...argList) {
        let [output] = argList;
        output.push({
            id: node.id,
            key: node.key
        });
    }

    protected onTraverseInOrderRec(node: K, ...argList) {
        let [counter] = argList;
        node.id = counter.increment();
    }

    protected onTraversePostOrderRec(node: K, ...argList) {
    }

    // Private Methods
    public assignInOrder(): void {
        this.traverseInOrder( new Counter() );
    }

    public toJsonPreOrder(): Array<ISerialization> {
        let output: Array<ISerialization> = [];
        this.traversePreOrder(output);
        return output;
    }

    // Public Methods
    public serialize(): Array<ISerialization> {
        this.assignInOrder();
        return this.toJsonPreOrder();
    }
}

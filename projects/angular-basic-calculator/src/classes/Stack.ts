export class Stack<T> {
    constructor() {
        this._stack = [];
    }

    // Private Variables
    private _stack: Array<T>;

    // Public Methods
    public push(val: T): void {
        this._stack.push(val)
    }

    public pop(): T {
        return this._stack.pop();
    }

    public peek(): T {
        return this._stack[this._stack.length - 1];
    }

    public print(): void {
        for (let i = this._stack.length - 1; i > -1; i--) {
            console.log(this._stack[i])
        }
    }

    public fromArray(fromArray: Array<T>):void {
        this._stack = fromArray;
    }

    public toArray(): Array<T> {
        return this._stack;
    }

    // Accessor
    public get isEmpty() {
        return this._stack.length < 1;
    }
}

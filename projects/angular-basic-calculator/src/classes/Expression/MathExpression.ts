export enum MathOperationEnum {
    PLUS = '+',
    MINUS = '-',
    MULTIPLY = '*',
    DIVIDE = '/',
    POWER = '^',
    OPEN_PARENTHESIS = '(',
    CLOSE_PARENTHESIS = ')',
}

export const MathOperationWeight = {};
// the minimum weight is 1, the higher the weight, the higher the precedence
MathOperationWeight[MathOperationEnum.PLUS] = 1;
MathOperationWeight[MathOperationEnum.MINUS] = 1;
MathOperationWeight[MathOperationEnum.MULTIPLY] = 2;
MathOperationWeight[MathOperationEnum.DIVIDE] = 2;
MathOperationWeight[MathOperationEnum.POWER] = 3;

const optionalSpace = '[\\s]*';
const anyOperator = `(${
    Object.keys(MathOperationEnum)
        .map(key => ['\\', ''].join(MathOperationEnum[key]))
        .join('|')
    })`;

const operatorBreak = ['', '\\B', ''].join(optionalSpace);

const leadingRepeatingOperator = `${anyOperator}${operatorBreak}`;
const trailingRepeatingOperator = `${operatorBreak}${anyOperator}`;

const nonCharBoundary = '([^\\s\\w\~!@#$%`\\={}\\[\\]";\\\'<>?,.:])'; // \\W or none of ~!@#$%_`={}[]\:";\'<>?,.
export const tokenRegExp = new RegExp(`${nonCharBoundary}|${leadingRepeatingOperator}|${trailingRepeatingOperator}`);

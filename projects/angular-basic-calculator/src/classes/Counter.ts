export class Counter {
    constructor() {
        this._cnt = 0;
    }

    // Private Variables
    private _cnt: number;

    // Public Methods
    public increment(): number {
        return ++this._cnt;
    }

    // Accessors
    get cnt(): number {
        return this._cnt;
    }
}

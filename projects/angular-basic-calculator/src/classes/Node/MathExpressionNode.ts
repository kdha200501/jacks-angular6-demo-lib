import { SerializableNode } from './SerializableNode';
import { Alphanumeric } from './AlphanumericNode';
import { MathOperationWeight } from '../Expression/MathExpression';

export class MathExpressionNode extends SerializableNode<Alphanumeric>{
    constructor(key?: Alphanumeric) {
        super(key);
    }

    // Accessors
    public get isOperand(): boolean {
        return this.isNull ? false : this.left.isNull;
    }

    public get isValid(): boolean {
        if (this.isNull) {
            return true;
        }

        return this.isOperand ? isFinite(<number>this.key) : !!MathOperationWeight[this.key];
    }
}

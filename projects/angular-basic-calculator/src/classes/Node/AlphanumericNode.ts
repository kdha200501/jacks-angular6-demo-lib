import { SerializableNode } from './SerializableNode';

export type Alphanumeric = string | number;

export class AlphanumericNode extends SerializableNode<Alphanumeric> {
    constructor(key?: Alphanumeric) {
        super(key);
    }
}

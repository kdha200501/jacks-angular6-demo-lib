import { Node } from './Node';

/*
    Compensate for Javascript's inability to pass an immutable null object to a function and have the function assign
    value to it.

    The solution is to mitigate actually passing a null node object to a function. To do so, a null node is represented
    as a node instance with null key.
*/
export class NullableNode<T> extends Node<T> {

    constructor(key: T = null) {
        super(key);
        if (this._key === null) {
            this._left = null;
            this._right = null;
        }
        else {
            this.initChildren();
        }
    }

    // Private methods
    private initChildren(): void {// mitigate actual null node situation
        this._left = new NullableNode();
        this._right = new NullableNode();
    }

    // Protected Methods
    public nullify(): void {
        this._key = null;
        this._left = null;
        this._right = null;
    }

    public overwriteKey(key: any): void {
        this._key = key;
        if (this._left === null) {
            this._left = new NullableNode();
        }
        if (this._right === null) {
            this._right = new NullableNode();
        }
    }

    // Accessors
    public get isNull(): boolean {
        return this.key === null;
    }

    public get key(): T {
        return this._key;
    }

    public set key(value: T) {
        this._key = value;
        this.initChildren();
    }

    public get left(): NullableNode<T> {
        return <NullableNode<T>>this._left;
    }

    public set left(value: NullableNode<T>) {
        this._left = value;
    }

    public get right(): NullableNode<T> {
        return <NullableNode<T>>this._right;
    }

    public set right(value: NullableNode<T>) {
        this._right = value;
    }
}

import { SerializableNode } from './SerializableNode';

export class NumericNode extends SerializableNode<number> {
    constructor(key?: number) {
        super(key);
    }
}

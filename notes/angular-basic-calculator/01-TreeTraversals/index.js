/*
global require
*/

const AlphanumericNode = require('../../build/Node/AlphanumericNode').AlphanumericNode;
const DemoTree = require('../../build/Tree/DemoTree').DemoTree;
const TraverseToSerialize = require('../../build/Traversal/TraverseToSerialize').TraverseToSerialize;

// Manually build a Binary Tree from child to parent
let h = new AlphanumericNode('H');
let c = new AlphanumericNode('C');
let j = new AlphanumericNode('J');

let a = new AlphanumericNode('A'); a.left = h; a.right = c;
let g = new AlphanumericNode('G'); g.left = j;
let f = new AlphanumericNode('F');
let k = new AlphanumericNode('K');

let d = new AlphanumericNode('D'); d.left = a; d.right = g;
let b = new AlphanumericNode('B'); b.left = f; b.right = k;

let root = new AlphanumericNode('E'); root.left = d; root.right = b;

// Print Tree
let tree = new DemoTree(); tree.root = root;
console.log(
    'tree height:', tree.height
);
tree.traverseToPrint.printLevelOrder();
tree.traverseToPrint.printPreOrder();
tree.traverseToPrint.printInOrder();
tree.traverseToPrint.printPostOrder();

// Serialize Tree
let serialization = tree.traverseToSerialize.serialize();
console.log(
    'serialize root with pre-order traversal:', serialization
);

// De-serialize to Tree
let treeClone = new DemoTree();
TraverseToSerialize.deserialize(treeClone.root, serialization);
console.log(
    'tree clone height:', treeClone.height
);
treeClone.traverseToPrint.printLevelOrder();
treeClone.traverseToPrint.printPreOrder();
treeClone.traverseToPrint.printInOrder();
treeClone.traverseToPrint.printPostOrder();

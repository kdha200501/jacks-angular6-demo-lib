![alt](../assets/pix03.bmp)

## 1. Intro

The `BST` data structure is to have such a storing repository that provides the efficient way of data sorting and searching.

## 2. Properties

- each node contains one key (also known as data)
- duplicate keys are not allowed
- insertion of key follows these rules:
  - `L < P` the keys in the left subtree are less then the key in its parent node
  - `P < R` the keys in the right subtree are greater the key in its parent node
- the sequence of insertion affects the height of the tree
- `in-order traversal` yields the order of ascending `key` value
- `pre-order traversal` yields the order nodes are inserted, originally

[eyeball] with *In Order Traversal* yields nodes in ascending order

```
4 6 8 10 15 18 21
```

[ref](https://www.cs.cmu.edu/~adamchik/15-121/lectures/Trees/trees.html)

## 3. Remove a node

- when a node is removed, its child nodes (if any) must be reassigned to an appropriate node

[eyeball] steps to removing the `12` node which has 2 child nodes:
![alt](../assets/bst-remove-case-3-3.png)

- find the next greater key *i.e.* the key of the in-order successor `19`
![alt](../assets/bst-remove-case-3-4.png)

- overwrite the key of `12` with the found key *i.e.* "19"
![alt](../assets/bst-remove-case-3-5.png)

- because the original `19` situates on a leave, it can be simply removed
![alt](../assets/bst-remove-case-3-6.png)

[ref](http://www.algolist.net/Data_structures/Binary_search_tree/Removal)

## 4. Conversion to Balanced `BST`

- the order of initial key insertion and the deletion of a key can alter the height of a `BST`
- a `BST` is unbalanced when the height of one side of the `BST` is significantly greater than the other side
- a balanced `BST` can be built from bisecting the `in-order traversal` of a unbalanced `BST`

## 5. Run TypeScript Demo

```shellscript

# output

insert keys: 5 2 -4 3 12 9 21 19 25

in-order traversal: -4,2,3,5,9,12,19,21,25
pre-order traversal: 5,2,-4,3,12,9,21,19,25

has 7: false
has 12: true

remove key: 12

in-order traversal: -4,2,3,5,9,19,21,25
pre-order traversal: 5,2,-4,3,19,9,21,25

insert keys: 26 27

left sub-tree height: 2
right sub-tree height: 5

balance Binary Search Tree

pre-order traversal: 19,3,2,-4,9,5,26,25,21,27
in-order traversal: -4,2,3,5,9,19,21,25,26,27

left sub-tree height: 3
right sub-tree height: 3
```

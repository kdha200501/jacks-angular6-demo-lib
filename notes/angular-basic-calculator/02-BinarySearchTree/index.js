/*
global require
*/

const BinarySearchTree = require('../../build/Tree/BinarySearchTree').BinarySearchTree;

let binarySearchTree = new BinarySearchTree();
let leftSubTree = new BinarySearchTree();
let rightSubTree = new BinarySearchTree();

// Build a BST
console.log('insert keys:', 5, 2, -4, 3, 12, 9, 21, 19, 25);
binarySearchTree.insertBatch(5, 2, -4, 3, 12, 9, 21, 19, 25);
leftSubTree.root = binarySearchTree.root.left;
rightSubTree.root = binarySearchTree.root.right;

// Traverse the BST
binarySearchTree.traverseToPrint.printInOrder();
binarySearchTree.traverseToPrint.printPreOrder();

// Search for keys in the BST
console.log(
    `has 7: ${
        !binarySearchTree.search(7).isNull
        }`
);
console.log(
    `has 12: ${
        !binarySearchTree.search(12).isNull
        }`
);

// Remove key from the BST
console.log('remove key:', 12);
binarySearchTree.remove(12);
binarySearchTree.traverseToPrint.printInOrder();
binarySearchTree.traverseToPrint.printPreOrder();

// Balance the BST
console.log('insert keys:', 26, 27);
binarySearchTree.insertBatch(26, 27);

// Height of the BST
console.log(
    `left sub-tree height: ${leftSubTree.height}`
);
console.log(
    `right sub-tree height: ${rightSubTree.height}`
);


// Balance the BST
console.log('balance Binary Search Tree');
binarySearchTree.balance();
leftSubTree.root = binarySearchTree.root.left;
rightSubTree.root = binarySearchTree.root.right;

// Traverse the balanced BST
binarySearchTree.traverseToPrint.printPreOrder();
binarySearchTree.traverseToPrint.printInOrder();

// Height of the balanced BST
console.log(
    `left sub-tree height: ${leftSubTree.height}`
);
console.log(
    `right sub-tree height: ${rightSubTree.height}`
);

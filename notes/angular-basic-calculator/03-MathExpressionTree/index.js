/*
global require
*/

const MathExpressionParser = require('../../build/Parser/MathExpressionParser').MathExpressionParser;

let infixExpression = '1 +2.2  *(   3.333^ 4- 55)';
console.log(
    'original Infix Expression:', infixExpression
);

let mathExpressionTree = new MathExpressionParser().parseInfixExpression(infixExpression);
console.log(
    'formatted Infix Expression:', mathExpressionTree.traverseToEvaluateMathExpression.format()
);
console.log(
    'Postfix Expression:', mathExpressionTree.traverseToPrint.exportPostOrder().map(node => node.key).join(' ')
);
console.log(
    'Postfix Expression evaluation:', mathExpressionTree.traverseToEvaluateMathExpression.solve()
);

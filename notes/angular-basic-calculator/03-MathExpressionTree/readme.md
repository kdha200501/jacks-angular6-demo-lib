![alt](../assets/pic11-300x259.png)

## 1. Intro
An *Infix* expression is a *declarative* way of expressing the order of command, which is more human-readable; An *Postfix* expression is an *imperative* way of expressing the order of command, which is more programming-friendly.

The traversal of an *Expression Tree* yields the order of operation execution. Each operation consists of two `operands` and an `operator`.

## 2. Properties

- it is a full `Binary Tree`, *i.e.* each node has either 0 or 2 children
- leaf nodes store operands *i.e.* numbers
- parent nodes are Math `operators`

[eyeball] with *In Order Traversal* yields an *Infix* expression which is human-readable

```
(5 * 4) + (100 - (20 / 2))
= 110
```

[recursion] with *Post Order Traversal* yields a *Postfix* expression which is useful for programming

```
[5 4 *] 100 20 2 / - +
20 100 [20 2 /] - +
20 [100 10 -] +
[20 90 +]
110
```

## 3. *Infix* to *Postfix* expression
![alt](../assets/kLrW3.png)

- re-order operators in an *Infix* expression to yield a *Postfix* expression
- tokenize an *Infix* expression and iterate through the tokens
- send `operands` to output
- push `operators` to a stack
- an `operator` with the highest precedence gets executed first, so pop all the `operators` of higher precedence to the output before pushing an `operator`
- push opening parenthesis to stack
- upon a closing parenthesis, pop all the `operators` on top of the first opening parentheses to the output
- upon `EOL`, pop all the remaining `operators`

## 4. *Postfix* expression to *Expression Tree*
![alt](../assets/slide_6.jpg)

- iterate the *Postfix* expression
- create node for `operand` and push to a stack
- create node for `operator`; pop to assign as right node; pop to assign as left node; push the `operator` node
- upon `EOL`, pop only remaining node

## 5. Run TypeScript Demo

```shellscript
# output

original Infix Expression: 1 +2.2  *(   3.333^ 4- 55)
formatted Infix Expression: (1+(2.2*((3.333^4)-55)))
Postfix Expression: 1 2.2 3.333 4 ^ 55 - * +
Postfix Expression evaluation: 151.49631259150627
```
